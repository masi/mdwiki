#!/usr/bin/env bash

# Markdown Wiki (mdwiki.sh)
# =========================
#
#  Copyright (c) 2021 Martin Singer <martin.singer@web.de>
#  Licensed under the terms of the GNU General Public License (GNU GPLv3+)
#
#
# About
# -----
#
#  Markdown Wiki is a system for decentralized stored, interconnected markdown quick notes.
#
#
# Requires
# --------
#
#  - `cat`
#  - `find`
#  - `git`
#
#
# Documentation
# -------------
#
#  - <https://www.gnu.org/software/bash/manual/bash.html#The-Shopt-Builtin>
#  - <https://git-scm.com/docs/git#Documentation/git.txt--Cltpathgt>

## Glob options.
shopt -s expand_aliases


## Defining global constants.
# readonly ifs_org="$IFS"

readonly prg="${0##*/}"
readonly version='0.1.0.0'
readonly config="$HOME/.config/mdwiki.ini"

readonly fallback_cmd_editor='nano'
readonly fallback_cmd_viewer='less'
# readonly fallback_cmd_editor='nvim'
# readonly fallback_cmd_viewer='glow -p'

readonly required_cmds=('cat' 'find' 'git')

readonly false=0
readonly true=1


## Declaring global variables with default values.
declare editor="$VISUAL"
declare viewer="$PAGER"

declare repo="$HOME/.local/share/mdwiki"
declare ext='.md'

declare verbose=$false
declare auto_git=$false


## Declaring alias commands
alias repogit='git -C "$repo"'


## Check for required programs.
function fn_check_for_required_programs()
{
	local req=("${required_cmds[@]}" "${editor%\ *}" "${viewer%\ *}")
	local ret=0

	for cmd in "${req[@]}"; do
		# echo >&2 "${prg}: TEST: checking for required command '$cmd'."  #TEST
		if ! $(hash "$cmd" 2> /dev/null); then
			echo >&2 "${prg}: ERROR: required command '$cmd' is not installed!"
			ret=1
		fi
	done

	if [ $ret -ne 0 ]; then
		exit $ret
	fi
}


## Check if the config file exists.
#
# @retval 0  config file is not readable
# @retval 1  config file is readable
function fn_check_config_file()
{
	if [ ! -r "$config" ]; then
		echo "$prg: Configuration file '$config' does not exist!"
		echo "$prg: Creating configuration file '$config'."

		# Set fallback commands, if required
		if [ -z "$editor" ]; then
			editor="$fallback_cmd_editor"
		fi

		if [ -z "$viewer" ]; then
			viewer="$fallback_cmd_viewer"
		fi

		return 1
	fi

	return 0
}


## Create a default config file.
function fn_create_default_config_file()
{
	cat > "$config" <<- EOF_CONFIG_FILE
	; Markdown Wiki ($prg) configuration file
	;
	;  - "editor": The document editor (e.g. "vim", "nano")
	;  - "viewer": The document viewer (e.g. "glow -p", "mdless")
	;  - "repo":   The directory to store the documents in
	;  - "ext":    The file extension for the documents (e.g. ".md", ".txt")

	[global]
	editor="$editor"
	viewer="$viewer"

	[doc]
	repo="$repo"
	ext="$ext"
	EOF_CONFIG_FILE

	echo "$prg: Please edit '$config' and restart '$prg'."
}


## Configuration parser.
#
#  Source: <https://gist.github.com/splaspood/1473761>
#
#  This function creates for every section
#  in the configuration file (.ini) a function.
#  E.g. `[global]` becomes `fn_cfg_section_global()`.
#
# @see fn_read_config_file()
#
# @param[in] $1  String, INI file name
# @param[in] $2  String, section prefix of created function
function fn_config_parser () {
	local fixed_file=$(sed 's/ *= */=/g' < $1)   # fix ' = ' to be '='
	local section_prefix="$2"

	IFS=$'\n' && local ini=( $fixed_file )       # convert file to line-array
	ini=( ${ini[*]//;*/} )                       # remove comments
	ini=( ${ini[*]/#[/\}$'\n'$section_prefix} )  # set section prefix
	ini=( ${ini[*]/%]/ \(} )                     # convert text2function (1)
	ini=( ${ini[*]/=/=\( } )                     # convert item to array
	ini=( ${ini[*]/%/ \)} )                      # close array parenthesis
	ini=( ${ini[*]/%\( \)/\(\) \{} )             # convert text2function (2)
	ini=( ${ini[*]/%\} \)/\}} )                  # remove extra parenthesis
	ini[0]=''                                    # remove first element
	ini[${#ini[*]} + 1]='}'                      # add the last brace

	eval "$(echo "${ini[*]}")"                   # eval the result (creating function)

	# echo "${ini[*]}"  #TEST
	# declare -p ini    #TEST
}


## Read the configuration file.
function fn_read_config_file()
{
	local prefix="fn_config_section_"

	# Call the parser
	fn_config_parser "$config" "$prefix"

	# Call the generated section function
	${prefix}global
	${prefix}doc
}


## Check if the repository directory exists.
#
#  Checks if the repository directory exists on the defined place.
#  If not, this function creates the directory.
function fn_check_repo_dir()
{
	# Check if the repository directory exists
	if [[ -d "$repo" ]]; then
		if [[ ! -w "$repo" ]]; then
			echo "$prg: ERROR: Repository directory '$repo' is not writable!"
			exit 1
		fi
	else
		echo "$prg: Repository directory '$repo' does not exist!"
		echo "$prg: Creating repository directory '$repo'"
		mkdir -p "$repo"

		[ $verbose -eq $true ] && echo >&2 "$prg: 'git init'"
		repogit init
	fi
}


## Evaluate the positional parameters.
#
#  This function evaluates the program parameters
#  and calls the corresponding function.
#
# @param[in] $@  All program parameters
function fn_evaluate_parameters()
{
	if [ -z "$1" ]; then
		echo >&2 "$prg: ERROR: No parameter '$1' was specified!"
		exit 1
	fi

	while [ -n "$1" ]; do
		case "$1" in
			-v | --verbose)
				verbose=$true
				;;
			-a | --auto-git)
				auto_git=$true
				;;
			-h | --help)
				fn_print_help
				exit 0
				;;
			-e | --edit)
				if [[ -z "$2" ]]
				then
					echo >&2 "$prg: ERROR: No document was specified!"
					exit 1
				fi
				fn_edit "${2%*${ext}}"
				shift
				;;
			-r | --read)
				if [[ -z "$2" ]]
				then
					echo >&2 "$prg: ERROR: No document was specified!"
					exit 1
				fi
				fn_view "${2%*${ext}}"
				shift
				;;
			-l | --list)
				fn_list
				exit 0
				;;
			-c | --contents)
				if [[ -z "$2" ]]
				then
					echo >&2 "$prg: ERROR: No document was specified!"
					exit 1
				fi
				fn_contents "${2%*${ext}}"
				shift
				;;
			-m | --move)
				if [[ -z "$2" ]]
				then
					echo >&2 "$prg: ERROR: No source document name was specified!"
					exit 1
				fi
				if [[ -z "$3" ]]
				then
					echo >&2 "$prg: ERROR: No target document name was specified!"
					exit 1
				fi
				fn_rename_sheet "${2%*${ext}}" "${3%*${ext}}"
				shift
				shift
				;;
			--git-remote)
				fn_set_git_remote_origin "$2"
				exit $?
				;;
			--pull | pull)
				fn_git_pull
				exit 0
				;;
			--push | push)
				fn_git_push
				exit 0
				;;
			*)
				echo >&2 "$prg: ERROR: Unknown parameter '$1'!"
				# fn_print_help
				# fn_edit "$1"
				# fn_view "$1"
				;;
		esac
		shift
	done
}


## Print `--help` message.
function fn_print_help()
{
	cat <<- EOF_HELP
	${prg} is a decentralized, personal, interconnected quick note system,
	using Markdown and Git.

	The configuration is stored in file '${config}'
	The documents are stored in directory '${repo}'.

	Usage: ${prg} [OPTION]... SHEET
	  -v, --verbose            verbose outputs
	  -a, --auto-git           automaticaly pull and push the git branch

	  -h, --help               display this help and exit
	  -e, --edit SHEET         edits and creates a cheat sheet
	  -r, --read               read a cheat sheet
	  -l, --list               lists cheat sheets
	  -c, --contents SHEET     shows the contents of a sheet
	  -m, --move SHEET SHEET   renames a source to a target sheet name

	      pull                 execute 'git pull' explicitly
	      push                 execute 'git push' explicitly
	      --git-remote ORIGIN  set 'git remote origin' remote repository

	Example: ${prg} -v sheet
	  '${cmd_viewer}' shows the content of file '${repo}/sheet${ext}'

	Requires:
	  * git
	  * cat
	  * find
	
	  * Editor: ${editor}
	  * Viewer: ${viewer}

	Exit status:
	  0 if OK
	  1 if an error occured

	Report ${prg} bugs to <martin_singer@mailbox.org>

	Copyright (c) 2022 Martin Singer <martin_singer@mailbox.org>

	Version: ${version}

	${prg} is distributed under the terms of the GNU General Public License
	as published by the Free Software Foundation, either version 3
	of the license, of (at your option) any later version.
	EOF_HELP
}


## Start the document editor.
#
# @param[in] $1  Sheet name
function fn_edit()
{
	# fn_git_pull  # AUTO remote
	local sheet="${1}${ext}"
	local path="${repo}/$sheet"

	# Edit if document exists and is a regular file ...
	# ... and if document exists and write permission is granted.
	# Create document if repository is a directory is writable
	if [[ -f "$path" ]] && [[ -w "$path" ]]; then
		[ $verbose -eq $true ] && echo "$prg: editing document '$path'"
		[ $auto_git -eq $true ] && fn_git_pull
		eval $editor "$path"
		repogit add "$path"
		repogit commit -m "$(date +"%F %H:%M%:::z"), $HOSTNAME, $prg edited '$sheet'" > /dev/null
		[ $auto_git -eq $true ] && fn_git_push
	elif [[ -d "$repo" ]] && [[ -w "$repo" ]]; then
		[ $verbose -eq $true ] && echo "$prg: new document '$path'"
		[ $auto_git -eq $true ] && fn_git_pull
		eval $editor "$path"
		repogit add "$path"
		repogit commit -m "$(date +"%F %H:%M%:::z"), $HOSTNAME, $prg added '$sheet'" > /dev/null
		[ $auto_git -eq $true ] && fn_git_push
	else
		echo >&2 "$prg: ERROR: Cannot edit or create '$path'"
	fi

	# Delete if document exists and is a regular file
	# and if document exists and has NO size greater than zero (is empty)
	if [[ -f "$path" ]] && [[ ! -s "$path" ]]; then
		if [[ -w "${repo}" ]]; then
			[ $verbose -eq $true ] && echo "$prg: delete empty document '$path'"
			[ $auto_git -eq $true ] && fn_git_pull
			# rm "${path}"
			repogit rm "${path}"
			repogit commit -m "$(date +"%F %H:%M%:::z"), $HOSTNAME, $prg removed '$sheet'" > /dev/null
			[ $auto_git -eq $true ] && fn_git_push
		else
			echo >&2 "$prg: WARNING: Cannot delete empty document '$path'"
		fi
	fi
	# fn_git_push  # AUTO remote
}


## Start the document viewer.
#
# @param[in] $1  Sheet name
function fn_view()
{
	# fn_git_pull  # AUTO remote
	local sheet="${1}${ext}"
	local path="${repo}/$sheet"

	if [[ -f "$path" ]] && [[ -r "$path" ]]; then
		[ $verbose -eq $true ] && echo "$prg: view document '$path'"
		eval $viewer "$path"
	else
		echo >&2 "$prg: Document '$path' was not found or is not readable"
	fi
}


## List content of a sheet.
#
# @param[in] $1  Path to sheet
function fn_content()
{
	local path="$1"
	mdcontent < "$path"
}


## Show information about a sheet.
#
# @param[in] $1  Sheet name
function fn_contents()
{
	local sheet="${1}${ext}"
	local path="${repo}/${sheet}"
	local info=""

	[ $auto_git -eq $true ] && fn_git_pull
	if [[ -f "$path" ]] && [[ -r "$path" ]]; then
		fn_content "$path"
	else
		echo >&2 "$prg: ERROR: Document '$path' was not found or is not readable!"
		exit 1
	fi
}


## List all sheets.
function fn_list()
{
	local list iter

	[ $auto_git -eq $true ] && fn_git_pull
	list="$(find -L "${repo}" -type f -name "*${ext}" | sort)"
	for iter in $list; do
		iter=${iter##*/}
		iter=${iter%${ext}}
#		echo -n "${iter%${ext}} "
		echo "${iter} "
	done | column
	echo
}


## Rename a sheet.
#
# @param[in] $1  Source sheet name
# @param[in] $2  Target sheet name
function fn_rename_sheet()
{
	[ $auto_git -eq $true ] && fn_git_pull
	local file_src="${1}${ext}"
	local file_tar="${2}${ext}"
	local path_src="$repo/$file_src"
	local path_tar="$repo/$file_tar"

	if [ ! -f "$path_src" ]; then
		echo >&2 "${prg}: ERROR: Source file '$path_src' can not be found!"
		exit 1
	fi

	if [ "$path_src" == "$path_tar" ]; then
		echo >&2 "${prg}: ERROR: Source file '$path_src' has the same name as the target sheet '$path_tar'!"
		exit 1
	fi

	list_paths="$(find -L "${repo}" -type f -name "*${ext}" | sort)"
	for iter_path in $list_paths; do
		if [ "$iter_path" == "$path_tar" ]; then
			echo >&2 "${prg}: ERROR: Target file '$path_tar' already exists!"
			exit 1
		fi
	done

	[ $verbose -eq $true ] && echo >&2 "$prg: Renming '$path_src' to '$path_tar'"
	repogit mv "$file_src" "$file_tar"
	[ $auto_git -eq $true ] && fn_git_push
}


## Setup the 'git remote origin' remote repository.
#
# @param[in] $1    git origin remote repository
# @retval       0  success
# @retval    $err  git error code
function fn_set_git_remote_origin()
{
	local origin="$1"
	if [ ! -z "$origin" ]; then
		repogit remote &> /dev/null
		if [ $? -eq 0 ]; then
			repogit remote get-url --all origin &> /dev/null
			if [ $? -eq 0 ]; then
				[ $verbose -eq $true ] && echo >&2 "$prg: 'git remote set-url origin $origin'"
				# repogit remote set-url origin "$origin"
				repogit remote set-url origin "$origin" && repogit pull --set-upstream origin master
				return $?
			else
				[ $verbose -eq $true ] && echo >&2 "$prg: 'git remote add origin $origin'"
				# repogit -C "$repo" remote add origin "$origin"
				repogit remote add origin "$origin" && repogit pull --set-upstream origin master
				return $?
			fi
		else
			echo >&2 "$prg: ERROR: No git repository found in '$path'!"
		fi
	else
		echo >&2 "$prg: ERROR: Parameter value '$origin' is invalid"
		return 1
	fi
	return 0
}


function fn_git_pull()
{
	repogit remote get-url --all origin &> /dev/null
	if [ $? -eq 0 ]; then
		[ $verbose -eq $true ] && echo >&2 "$prg: 'git pull'"
		repogit pull
	fi
}


function fn_git_push()
{
	repogit remote get-url --all origin &> /dev/null
	if [ $? -eq 0 ]; then
		[ $verbose -eq $true ] && echo >&2 "$prg: 'git push'"
		repogit push
	fi
}


## Main function.
#
# @param[in] $@
function fn_main()
{
	fn_check_config_file
	if [ $? -ne 0 ]; then
		fn_create_default_config_file
	fi

	fn_read_config_file
	fn_check_for_required_programs
	fn_check_repo_dir
	fn_evaluate_parameters "$@"
}


fn_main "$@"
exit 0
