#!/usr/bin/gawk -f


function count_char_in_string(str, char) {
	len_str_full = length(str)
	gsub(char, "", str)
	len_str_sub = length(str)
	return len_str_full - len_str_sub
}


function print_string_multiple(str, num) {
	while (num-- > 0) {
		printf str
	}
}


function print_chapter(depth, title) {
	if (1 == depth) {
		print_string_multiple("  ", depth)
		print bc_black fc_blue ff_reverse title format_reset
#		print ""
	}
	else if (2 == depth) {
#		print ""
		print_string_multiple("  ", depth)
		print bc_black fc_blue title format_reset
	}
	else {
		print_string_multiple("  ", depth)
		print title
	}
}


#function printc(str, fg, bg, 


BEGIN {
#	print "BEGIN"

	# Format and Color Reset
	format_reset   = "\033[0;0m"

	# Font Format
	ff_nomral       = "\033[0m"
	ff_bold         = "\033[1m"
	ff_darker       = "\033[2m" # seems to make colors darker
	ff_italic       = "\033[3m"
	ff_underline    = "\033[4m"
	ff_blink        = "\033[5m" # seems not to work
	ff_test         = "\033[6m" # ???
	ff_reverse      = "\033[7m" # switches fore and background colors

	# Font Color
	fc_black        = "\033[0;30m"
	fc_red          = "\033[0;31m"
	fc_green        = "\033[0;32m"
	fc_brown        = "\033[0;33m"
	fc_blue         = "\033[0;34m"
	fc_purple       = "\033[0;35m"
	fc_cyan         = "\033[0;36m"
	fc_light_gray   = "\033[0;37m"

	# Font Color (for bold fonts)
	fc_dark_gray    = "\033[1;30m"
	fc_light_red    = "\033[1;31m"
	fc_light_green  = "\033[1;32m"
	fc_yellow       = "\033[1;33m"
	fc_light_blue   = "\033[1;34m"
	fc_light_purple = "\033[1;35m"
	fc_light_cyan   = "\033[1;36m"
	fc_white        = "\033[1;37m"

	# Background Color
	bc_black        = "\033[40m"
	bc_red          = "\033[41m"
	bc_green        = "\033[42m"
	bc_brown        = "\033[43m"
	bc_blue         = "\033[44m"
	bc_purple       = "\033[45m"
	bc_cyan         = "\033[46m"
	bc_gray         = "\033[47m"
}


{
	if (/^#/) {

		depth = count_char_in_string($1, "#") # counts the '#' characters in the first field

		gsub(/^#+/, "", $0) # removes all leading '#' characters
		gsub(/^ +/, "", $0) # removes all leading ' ' characters (between leading '#' and text)

		gsub(/#+$/, "", $0) # removes all closing '#' characters
		gsub(/ +$/, "", $0) # removes all closing ' ' characters (between text and closing '#')

		title = $0

		print_chapter(depth, title)
	}
	else if (/^=/) {
		print_chapter(1, line_buffer)
	}
	else if (/^-/ && ! /\|/ && ! / /) {
		print_chapter(2, line_buffer)
	}
	else {
		line_buffer = $0 # stores line
	}
}


#END {
#	print "END"
#}

