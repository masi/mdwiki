BINDIR = /usr/local/bin
PRG1   = mdwiki
PRG2   = mdcontent
RM     = rm -f

mdwiki:
	

install:
	install --mode=755 $(PRG1).sh  $(BINDIR)/$(PRG1)
	install --mode=755 $(PRG2).awk $(BINDIR)/$(PRG2)

uninstall:
	$(RM) $(BINDIR)/$(PRG1)
	$(RM) $(BINDIR)/$(PRG2)
