Markdown WIKI
=============

Markdown WIKI (mdwiki.sh) is a decentralized, personal, interconnected quick note system,
using Markdown and Git.

Copyright (c) 2022 Martin Singer <martin_singer@mailbox.org>


License
-------

mdwiki.sh is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

mdwiki.sh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or RITNES FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>


About
-----

### `mdwiki.sh`

 `mdwiki` is a shell (`bash`) script which offers an interface to personal cheat sheets.
 Markdown WIKI are a kind of manual pages for personal, interconnected quick notes.
 The sheets are stored in directory `~/.mdwiki`.

 The wiki pages are generic ASCII text files with the suffix `.md`
 and are designated to be formated in Markdown.


### `mdcontent.awk`

 `mdcontent` is a awk (`gawk`) script, listing the content index of a markdown document.


Requires
--------

### `mdwiki.sh`

 - `cat`
 - `find`
 - `git`


### `mdcontent.awk`

 - `gawk`


Installation
------------

 Install

 ```SHELL
 $ sudo make install
 ```


 Uninstall

 ```SHELL
 $ sudo make uninstall
 ```


Usage
-----

### `mdwiki.sh`

 - The configuration is stored in file `~/.config/mdwiki.ini`
 - The documents are stored by default in directory `~/.local/share/mdwiki`

```shell
-h, --help               display this help and exit
-e, --edit SHEET         edits and creates a cheat sheet
-v, --view               view a cheat sheet
-l, --list               lists cheat sheets
-i, --info SHEET         shows the information line of a sheet

    --pull               execute 'git pull' explicitly
    --push               execute 'git push' explicitly
    --git-remote ORIGIN  set 'git remote origin' remote repository
```


#### Setup

 - Using a git server
   1. Setup the git remote repository for the documents on a server
   2. Execute `mdwiki.sh --git-remote 'git@<server>:<user>/<repository>.git'`
   3. Edit the configuration file `~/.config/mdwiki.ini`

 - Local only
   1. Execute `mdwiki.sh`, without parameters
   2. Edit the configuration file `~/.config/mdwiki.ini`


### `mdcontent.awk`

 ```SHELL
 $ mdcontent <document>
 ```


Moreover
--------

 - Markdown renderer for the CLI:
   - <https://github.com/charmbracelet/glow>
   - <https://aur.archlinux.org/packages/glow/>
   - Usage: `glow -p SOURCE`

 - Markdown renderer for Firefox:
   - <https://addons.mozilla.org/en-US/firefox/addon/markdown-viwer/>
